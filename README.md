Building JavaScript with Gulp
=============================

### Requirements

1. Install Node.js

#### Step 1

Install gulp as a global module on your computer

```
sudo npm install -g gulp
```

#### Step 2

Install the various gulp plugins

```
cd my-project
npm install
```

#### Step 3

Run gulp CLI from project root. This will look at your gulpfile.js and run all the tasks in that file that you set up.

```
gulp
``` 

This will produce the folder build/ with your new HTML files

### References

* [Gulp Site](http://gulpjs.com/)
* [Node Package Manager](https://www.npmjs.org/)
