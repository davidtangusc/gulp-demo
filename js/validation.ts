/**
 * This functions tests whether a given email is valid or not. It checks
 * if the @ symbol exists.
 * 
 * @module  window
 * @method  validateEmail
 * @param  {string} email The email address you want to validate
 * @return {boolean}       Whether the email address was valid or not
 */
var validateEmail = function(email) {
  if (email.indexOf('@') !== -1) {
    return true;
  }

  return false;
};

/**
 * This function tests whether a given phone number is valid or not.
 * It checks if the there is 10 digits.
 * 
 * @module  window
 * @method  validatePhoneNumber
 * @param  {string} phoneNumber 10 digit phone number
 * @return {boolean}             True if valid phone number, false otherwise
 */
var validatePhoneNumber = function(phoneNumber) {
	return true;
};