/**
 * This function creates a slideshow using some cool jQuery plugin
 * 
 * @method  slideshow
 * @param  {string} id The ID of the element you want the slideshow to be in
 * @return {object}    Returns the jQuery slideshow object
 */
function slideshow(id) {
	$('#' + id).slideshow({
		/* slideshow options here */
	});
}