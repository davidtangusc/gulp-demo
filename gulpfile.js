/**
 * Include all the proper plugins for our build process
 * We are including gulp plugins for:
 * file concatenation (gulp-concat)
 * minification (gulp-uglify)
 * and script tag replacement (gulp-html-replace)
 */
var gulp = require('gulp');
// var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
// var htmlreplace = require('gulp-html-replace');
var ts = require('gulp-typescript');
var angularFilesort = require('gulp-angular-filesort');
var inject = require('gulp-inject');
var watch = require('gulp-watch');

function build() {
	var tsResult = gulp
		.src(['js/**/*.ts'])
		.pipe(ts({
			out: 'output.js'
		}))
		.js
		// .pipe(uglify())
		// .pipe(angularFilesort())
		.pipe(gulp.dest('dist'));
}

gulp.task('default', function() {
	build();


	/** 
	 * Update your index page (or other pages too)
	 * with references to the build JavaScript file
	 */
	// gulp.src('index.html')
	// 	.pipe(htmlreplace({
	// 		'js': '../dist/itp460.js'
	// 	}))
	// 	.pipe(gulp.dest('build'));
});

gulp.task('dev', function() {
	watch('js/**/*.ts', function() {
		build();
		console.log('Build at: ' + Date.now());
	});
});

